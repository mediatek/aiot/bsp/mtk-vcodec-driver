# mtk-vcodec-driver

VCODEC is the proprietary video encode & video decode hardware driver in Mediatek SoC.

This repo is out-of-tree kernel driver of VCODEC. 


## Branch

- The main branch is unused. 

- For i1200 platform: use `mt8395` branch. The kernel driver is compatible with [mtk-v5.15-dev](https://gitlab.com/mediatek/aiot/bsp/linux/-/tree/mtk-v5.15-dev) and it is still under development and subject to design changes.

